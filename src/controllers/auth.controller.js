const bcrypt = require('bcrypt')
const { generateToken } = require('../utils/jwt.token.util')
const { responseSuccess, responseError } = require('../utils/response.formatter.util')
const { User } = require('../../models')

module.exports = {
    login: async (req, res) => {
        const { uniqueNumber } = req.body                
        try {
            const userLogin = await User.findOne({ where: { uniqueNumber} })

            if (!userLogin) {
                res.status(400).json(responseError('User not found'))
            } else {                
                const token = await generateToken(userLogin.id)
                const responseUser = {
                    id: userLogin.id,
                    username: userLogin.username,
                    token: "Bearer " + token
                }
                res.json(responseSuccess(responseUser))                
            }
        } catch (err) {
            console.log(err.message)
            res.status(404).json(responseError(err.message))
        }
    }
}