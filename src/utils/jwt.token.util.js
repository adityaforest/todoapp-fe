const jwt = require('jsonwebtoken')

const { User } = require('../../models')

module.exports = {
    generateToken: async (id) => {
        const users = await User.findByPk(id)

        const token = jwt.sign(
            {
                id: users.id,                
                username: users.username                
            },
            process.env.JWT_SECRET_KEY,
            {
                expiresIn: '24h'
            }
        )

        return token;
    },
    getUserVerified: (token) => {
        if (token) {
            try {
                verify = jwt.verify(token, process.env.JWT_SECRET_KEY)
                return verify
            } catch (err) {
                return false
            }
        }

        return false

    },
    getTokenFromHeader: (token) => {
        return token.split(' ')[1]
    }
}