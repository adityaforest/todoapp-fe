const { getUserVerified, getTokenFromHeader } = require('../utils/jwt.token.util')

const { responseError } = require('../utils/response.formatter.util')

module.exports = {
    restricted: (req, res, next) => {
        const reqAuth = req.headers["authorization"]

        if (reqAuth) {
            const token = getTokenFromHeader(reqAuth)
            if (getUserVerified(token)) next()
        } else {
            res.status(401).json(responseError('Unauthorized'))
        }

    }
}