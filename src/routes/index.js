const express = require('express')
const router = express.Router()

const taskRoute = require('./task.route')
const authRoute = require('./auth.route')

const { restricted } = require('../middlewares/restrict.middleware')

// Open API routes
router.use('/api/v1', authRoute)
// Closed API routes (must include authorization header with valid token)
router.use(restricted)
router.use('/api/v1', taskRoute)

module.exports = router
