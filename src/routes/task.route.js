const express = require('express')
const router = express.Router()

const taskController = require('../controllers/task.controller')

// Authentication
router.get('/tasks/getAll/:user_id', taskController.getAll)
router.post('/tasks/create' , taskController.create)
router.put('/tasks/update/:task_id' , taskController.edit)
router.delete('/tasks/delete/:task_id', taskController.delete)

module.exports = router